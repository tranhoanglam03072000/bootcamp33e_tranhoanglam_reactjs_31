import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
export default class Glasses extends Component {
  data = dataGlasses;
  state = {
    model: {
      url: "",
      name: "",
      price: "",
      desc: "",
    },
  };
  style = {
    backGround: {
      backgroundImage: `url(./glassesImage/background.jpg)`,
      width: "100vw",
      height: "100vh",
      backgroundSize: "cover",
    },
    backGroundContent: {
      backgroundColor: "rgba(0,0,0,0.7)",
    },
  };
  handelGlasses = (item) => {
    document.getElementById("model__content").style.display = "block";
    this.setState({
      model: {
        url: `${item.url}`,
        name: `${item.name}`,
        price: `${item.price}`,
        desc: `${item.desc}`,
      },
    });
  };
  renderGlasses = () => {
    return this.data.map((item, index) => {
      return (
        <img
          onClick={() => this.handelGlasses(item)}
          className="p-5"
          key={index}
          src={item.url}
          width={200}
          alt=""
        />
      );
    });
  };
  render() {
    return (
      <div>
        <div style={this.style.backGround}>
          <div className="container mx-auto">
            <div className="flex justify-center">
              <div className="model relative">
                <img src="./glassesImage/model.jpg" alt="" width={300} />
                <img
                  className="absolute top-24 left-16"
                  src={this.state.model.url}
                  alt=""
                  width={180}
                />
                <div
                  style={this.style.backGroundContent}
                  id="model__content"
                  className="model__content absolute inset-x-0 bottom-0 h-32 hidden"
                >
                  <p className="text-blue-400 text-2xl mb-5">
                    {this.state.model.name} - {this.state.model.price} $
                  </p>
                  <p className="text-white">{this.state.model.desc}</p>
                </div>
              </div>
            </div>
            <div
              className="grid grid-cols-6 gap-3 mt-10"
              style={{ background: "white" }}
            >
              {this.renderGlasses()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
